git pull
docker-compose -f production.yml down
docker-compose -f production.yml build
docker-compose -f production.yml up -d
docker-compose -f production.yml run --rm django python manage.py migrate
docker-compose -f production.yml run --rm django python manage.py collectstatic --noinput
docker update --restart unless-stopped $(docker ps -q)